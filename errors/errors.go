package main

import (
	"errors"
	"fmt"
	"io/ioutil"
)

func MyFunc(condition bool) (int, error) {
	if condition {
		return 42, nil
	}

	return 0, errors.New("Error!")
}

func readFile(filename string) (string, error) {
	dat, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}

	if len(dat) == 0 {
		return "", fmt.Errorf("Empty content (filename=%v)", filename)
	}

	return string(dat), nil
}

func main() {
	res, err := MyFunc(true)
	fmt.Println(res, err)

	res, err = MyFunc(false)
	fmt.Println(res, err)

	dat, err := readFile("test.txt")
	if err != nil {
		fmt.Printf("Error while reading file: %v\n", err)
		return
	}

	fmt.Println("File content:")
	fmt.Println(dat)
}
