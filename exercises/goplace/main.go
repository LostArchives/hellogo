package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

const inputfile = "input/wikigo.txt"
const outputfile = "output/wikigo.txt"
const oldtext = "Go "
const newtext = "Python "

func FindReplaceFile(src, dst, old, new string) (occ int, lines []int, err error) {
	srcfile, err := os.Open(src)
	if err != nil {
		return occ, lines, err
	}
	defer srcfile.Close()

	dstfile, err := os.Create(outputfile)
	if err != nil {
		fmt.Printf("Error while creating output file : %v\n", err)
		return occ, lines, err
	}
	defer dstfile.Close()

	numline := 1
	scanner := bufio.NewScanner(srcfile)
	writer := bufio.NewWriter(dstfile)
	defer writer.Flush()

	for scanner.Scan() {
		t := scanner.Text()
		found, res, curocc := ProcessLine(t, old, new)
		if found {
			occ += curocc
			lines = append(lines, numline)
		}
		fmt.Fprintln(writer, res)
		fmt.Println(res)
		numline++
	}
	return occ, lines, nil
}

func ProcessLine(line, old, new string) (found bool, res string, occ int) {
	res = line
	oldlower := strings.ToLower(old)
	newlower := strings.ToLower(new)
	if strings.Contains(line, old) || strings.Contains(line, oldlower) {
		found = true
		occ += strings.Count(line, old)
		occ += strings.Count(line, oldlower)
		res = strings.Replace(line, old, new, -1)
		res = strings.Replace(res, oldlower, newlower, -1)
	}
	return found, res, occ
}

func main() {
	occ, lines, err := FindReplaceFile(inputfile, outputfile, oldtext, newtext)
	if err != nil {
		fmt.Printf("Error while replacing : %v", err)
		return
	}
	fmt.Println("== Summary ==")
	fmt.Printf("Number of occurrences of Go : %d\n", occ)
	fmt.Printf("Number of lines : %d\n", len(lines))
	fmt.Printf("Lines : %v\n", lines)
	fmt.Println("== End of Summary ==")
}
