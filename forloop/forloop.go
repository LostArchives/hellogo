package main

import "fmt"

func main() {
	sum := 0
	for i := 0; i < 5; i++ {
		sum += i
	}
	fmt.Printf("Sum value=%d\n", sum)

	eventCnt := 0
	for eventCnt < 3 {
		fmt.Println("Retrieving events...")
		eventCnt++
		if eventCnt == 3 {
			fmt.Printf("Got %d notifications, updating\n", eventCnt)
		}
	}

	i := 0
	for {
		i++
		if i%2 != 0 {
			fmt.Println("Odd looping")
			continue
		}
		fmt.Println("Looping...")

		if i >= 10 {
			fmt.Println("Loop end")
			break
		}
	}
}
