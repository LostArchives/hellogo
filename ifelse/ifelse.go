package main

import (
	"fmt"
)

func main() {
	day := 5

	if day < 15 {
		fmt.Printf("first half of the month (day=%d)\n", day)
	} else if day == 18 {
		fmt.Printf("sêcial day (day=%d)\n", day)
	} else {
		fmt.Printf("second half of the month (day=%d)\n", day)
	}

	year, month, day := 2018, 11, 10
	fmt.Printf("Date=%d/%d/%d\n", day, month, year)

	if year == 2009 && month == 11 && day == 10 {
		fmt.Println("This is the first release of GO!")
	} else if year == 2009 || month == 11 || day == 10 {
		fmt.Println("At least one part is right... :/")
	} else {
		fmt.Println("Just another day...")
	}

	if count := 12; count > 10 {
		fmt.Printf("we have enough count. got=%d\n", count)
	} else {
		fmt.Printf("Not enough. got=%d\n", count)
	}

}
