package main

import (
	"fmt"
	"time"
)

func client1(c chan string) {
	for i := 0; i < 5; i++ {
		c <- fmt.Sprintf("Message from client1 => %v", i)
		time.Sleep(1500 * time.Millisecond)
	}
}

func client2(c chan string) {
	for i := 0; i < 10; i++ {
		c <- fmt.Sprintf("Message from client2 => %v", i)
		time.Sleep(1500 * time.Millisecond)
	}
}

func main() {
	cl := make(chan string)
	c2 := make(chan string)
	go client1(cl)
	go client2(c2)

	maxEmpty := 10
	currEmpty := 0

	for currEmpty <= maxEmpty {
		time.Sleep(1000 * time.Millisecond)
		select {
		case v := <-cl:
			fmt.Println("Received from client1: ", v)
		case v := <-c2:
			fmt.Println("Received from client2: ", v)
		default:
			fmt.Println("No value received")
			currEmpty++
		}
	}

}
